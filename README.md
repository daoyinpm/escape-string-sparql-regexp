# Escape String Sparql Regexp

SPARQL 裡如果有用到 REGEX() 時，其字串代入時需要處理跳脫字元，但是它又與一般處理 regex 的跳脫字元不同。
一般：'(' => '\('
SPARQL: '(' => '\\('

## Install

```
$ npm install escape-string-sparql-regexp
```

## Usage

```js
import escapeStringSparqlRegexp from 'escape-string-sparql-regexp';

const escapedString = escapeStringSparqlRegexp('This is (a) test.');
//=> 'This is \\(a\\) test.'
