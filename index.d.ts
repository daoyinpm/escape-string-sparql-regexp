/**
 Escape SPARQL RegExp special characters.

 You can also use this to escape a string that is inserted into the middle of a regex, for example, into a character class.
 */
export default function escapeStringSparqlRegexp(string: string): string;
